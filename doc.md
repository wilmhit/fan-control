The GPLv3 License (GPLv3)

Copyright (c) 2022 Jakub Chodubski

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Fan-Control source code documentation

## 0. Table of contents

 1. Source files and compatibility
 2. Build system
 3. Constants
 4. Global variables
 5. Structs 
 6. Code overview
 7. MISRA C compatibility

## 1. Source files and compatibility

Program consists of one `.c` source file. It is located in `src` directory.
It has been written to be compatible with GNU C library (glibc) running on a
POSIX compatible system. While running it with different environment may be
possible, it has not been tested or proved. Changing C library or platform may
result in undefined behaviours. Run on your own risk.

## 2. Build system

Program is compiled with use of CMake. Before using it make sure to have
installed:
 - GNU Compiler Collection (gcc), version 11 or later
 - CMake build system, version 3.11 or later
 - GNU Make, version 4 or later

If you have those components installed then building should be as simple as:

 1. Creating `build` directory in root directory of the project: `mkdir build`
 2. `cd build`
 3. `cmake ..`
 4. `cmake --build .`

And consecutive builds can be done only with `cmake --build` while working
directory is still `build`.

## 3. Constants

File `main.c` below *include* lines contains a list of constant parameters
defined as precompiler macros. Those are prepared to be changed by users before
actual compilation. These macros are:

 - CONTROL1 - This is a filepath of a control file for the first sensor. This
   file will be written to during operation of the program. It's contents will
   be either 1 or 0.
 - CONTROL2 - Same as above but for second sensor.
 - CONTROL3 - Same as above but for third sensor.
 - TEMP1 - This is a filepath of a temperature file containing a temperature
 readout from the first sensor. During the operation of the program this file
 will be periodically read. This file should contain values ranging between 0
 and 255.
 - TEMP2 - Same as above but for second sensor.
 - TEMP3 - Same as above but for third sensor.
 - THRESHOLD\_TEMP - This is a integer value symbolizing a threshold value at
 which fan should be enabled for all sensors. Should range between 0 and 255.
 - SECONDS\_REFRESH - Integer value for seconds to wait between checking again
 temperature at sensors.

## 4. Global variables

Program uses 2 global variables and both are used to control errors:

 - *global_error_description* is a variable that will store description of any
 error that can occur during runtime.
 - *memory_error* is allocated on start of the program and is used as a
 placeholder description if a proper description can not be allocated.

## 5. Structs 

Struct *Sensor* is used to keep pointers to file descriptors of any sensor. It
also stores threshold temperature value for any sensor separately, as this
might be useful for future feature expansion of the program.

## 6. Code overview

#### Main function

The *main* function is the entry point of the program. It consists of a loop
that in successful scenario should run endlessly. Loop breaks if either an
error occurred during previous run or process received TERM signal.

With every iteration function zeroes a memory needed for storing all 3 *Sensor*
structs. Next sensors are read (which opens a file descriptors for all files).
When a valid temperature is read out control files are written to. All
temperatures are printed. After that file descriptors are deinitialized in
order to allow any outside program to operate on files.

At this point errors are checked. If an error did occur then loop breaks and
error description is printed.

If no error is detected loop waits chosen amount of time before continuing with
next iteration

#### Error handling

All error handling in code relies on 2 functions:

 - *error(char\*, va_list)* which is a wrapper for *vsnprintf* standard library
 function. Formatted strings are written to *global_error_description*.
 - *is_error()* which returns a boolean-like integer value as the name
 suggests.

## 7. MISRA C compatibility

This program does not comply with all strict MISRA requirement and suggestions.
All violations are marked in code with appropriate cpp-check suppress comment.

Full MISRA compliance can not be achieved as essential part of the program is
reading and writing to a file. This requires use of Standard IO functions that
are discouraged.

Most of the MISRA violations have been explained within the code with a
comment.

