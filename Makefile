default:
	watch cppcheck --addon=misra_addon.json --inline-suppr src/main.c

run:
	cd build && cmake --build . > /dev/null
	sensor --input con1 --output temp1 > /dev/null &
	sensor --input con2 --output temp2 > /dev/null &
	sensor --input con3 --output temp3 > /dev/null &
	sleep 0.1 && ./build/src/fan-control &
	clear && echo "Press enter to stop "&& read
	kill $$(ps | grep " sensor" | cut -d " " -f 2) &> /dev/null || echo "$$(tput setaf 1)No sensor process"
	kill $$(ps | grep " fan-control" | cut -d " " -f 2) &> /dev/null || echo "$$(tput setaf 1)No fan control process"
	
