#include <errno.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h> // cppcheck-suppress misra-c2012-21.6; stdio is the only
                   // way to read a file. Temperature is stored in a file.
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

#ifdef _WIN32
#include <Windows.h>
#else
#include <unistd.h>
#endif

// Filepaths for control and temperature files
#define CONTROL1 "con1"
#define CONTROL2 "con2"
#define CONTROL3 "con3"
#define TEMP1 "temp1"
#define TEMP2 "temp2"
#define TEMP3 "temp3"

#define TRESHOLD_TEMP 140
#define SECONDS_REFRESH 3

//***********************//
//       STRUCTS         //
//***********************//

typedef struct Sensor {
  int temperature;
  FILE *control_file;
  u_int8_t treshold_temp;
} sensor;

//***********************//
// FUNCTION DECLARATIONS //
//***********************//

void error(const char *text, ...);
int is_error(void);
static char *read_file_contents(FILE *file_desc);
const char *convert_to_byte(const char *file_contents);
const char *file_read(const char *filename);
int convert_to_temp(const char *number);
static void deinit_sensors(sensor *sensors);
static void read_sensor(sensor *sensors);
static void steer_sensors(sensor *sensors);
static void turn_off(FILE *file);
static void turn_off(FILE *file);

//***********************//
//        GLOBALS        //
//***********************//

// cppcheck-suppress misra-c2012-8.4
char *global_error_description = NULL;

// cppcheck-suppress misra-c2012-8.4
const char *memory_error =
    "Unable to display error message"; // This should be allocated on start so
                                       // at least this error can be produced in
                                       // case of corrupted memory

//***********************//
//      FUNCTIONS        //
//***********************//

void error(const char *text, ...) {
  // All these suppressed warnings are here because of va_list which shall not
  // be used. However since this is a wrapper around vsnprintf(..) function
  // that uses va_list and that is also not allowed the use of this macro is
  // unavoidable. This should normally report errors to some other component of
  // the system.
  va_list arg_list;         // cppcheck-suppress misra-c2012-17.1
  va_start(arg_list, text); // cppcheck-suppress misra-c2012-17.1
  int formatting_error =
      vsnprintf(global_error_description, 100, text, arg_list);
  va_end(arg_list); // cppcheck-suppress misra-c2012-17.1
  if (formatting_error < 0) {
    // cppcheck-suppress misra-c2012-11.8
    global_error_description = (char *)memory_error;
  }
}
int is_error(void) { return (global_error_description != NULL); }

static char *read_file_contents(FILE *file_desc) {
  // cppcheck-suppress [misra-c2012-11.5, misra-c2012-21.3]
  char *buffer = malloc(10);
  unsigned long bytes_read = fread(buffer, 1, 10, file_desc);
  if (bytes_read == (unsigned long)0) {
    error("There was an error while reading file");
  }
  return buffer;
}

const char *file_read(const char *filename) {
  const char *file_contents = NULL;
  int error_while_closing = 0;
  FILE *file_ptr = NULL;

  errno = 0;
  file_ptr = fopen(filename, "r");
  if (errno != 0) { // cppcheck-suppress misra-c2012-22.10; Warning was
                    // suppressed because this warning is an linter bug (due to
                    // suppressed #include <stdio.h>)
    error("ERRNO %d while opening file %s", errno, filename);
  }
  if (is_error() == 0) {
    file_contents = read_file_contents(file_ptr);
  }
  error_while_closing = fclose(file_ptr);
  if (error_while_closing != 0) {
    error("Error while closing file %s", filename);
  }
  return file_contents;
}

int convert_to_temp(const char *number) {
  // Writing own conversion function is a dawnting task
  // cppcheck-suppress misra-c2012-21.7; atoi not allowed
  return atoi(number);
}

void read_sensor(sensor *sensors) {
  sensors[0].temperature = convert_to_temp(file_read(TEMP1));
  sensors[1].temperature = convert_to_temp(file_read(TEMP2));
  sensors[2].temperature = convert_to_temp(file_read(TEMP3));

  for (int i = 0; i < 3; i++) {
    sensors[i].treshold_temp = TRESHOLD_TEMP;
  }

  sensors[0].control_file = fopen(CONTROL1, "r+");
  sensors[1].control_file = fopen(CONTROL2, "r+");
  sensors[2].control_file = fopen(CONTROL3, "r+");
}

static void deinit_sensors(sensor *sensors) {
  for (int i = 0; i < 3; i++) {
    if (fclose(sensors[i].control_file) != 0) {
      error("Error closing file!");
    }
  }
}

static void turn_on(FILE *file) {
  if (fwrite("0", 1, 1, file) != (size_t)1) {
    error("Could not write to file %s");
  }
}

static void turn_off(FILE *file) {
  if (fwrite("1", 1, 1, file) != (size_t)1) {
    error("Could not write to file %s");
  }
}

static void steer_sensors(sensor *sensors) {
  for (int i = 0; i < 3; i++) {
    if (sensors[i].temperature > sensors[i].treshold_temp) {
      turn_on(sensors[i].control_file);
    } else {
      turn_off(sensors[i].control_file);
    }
  }
}

int main(void) {
  sensor sensors[3] = {0};
  while (1) {
    memset(sensors, 0, 3 * sizeof(sensor));
    read_sensor(sensors);
    steer_sensors(sensors);
    if (printf("Temps: %d, %d, %d\n", sensors[0].temperature,
               sensors[1].temperature, sensors[2].temperature) < 0) {
      error("Printf failed");
    }
    deinit_sensors(sensors);
    if (is_error() == 1) {
      break;
    }
    sleep(SECONDS_REFRESH);
  }
  if (is_error() == 1) {
    if (printf("Error %s", global_error_description) < 0) {
      error("Printf failed");
    }
  }
  return is_error();
}
